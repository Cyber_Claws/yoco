import React, { Component } from 'react';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prices: [],
      screenamount: '0'
    }
  }

  handlePress = prop => event => {
    const {screenamount} = this.state;
    if(parseFloat(screenamount) > 1000000) {
      return // Error
    }
    this.setState({ screenamount: 
      screenamount+prop
    });
  }

  handleAppend() {
    let {prices, screenamount} = this.state;
    if(parseFloat(screenamount) === 0) {
      return;
    }
    prices.push(screenamount)
    this.setState({
      prices,
      screenamount: '0'
    })
  }

  handleDelete(index) {
      let { prices } = this.state;
      // TODO prices = prices.splice(index, 1)
      this.setState({
        prices
      })
  }

  handlePop() {
    let {screenamount} = this.state;
    screenamount = screenamount.slice(0, -1);
    this.setState({
      screenamount: screenamount || "0"
    })
  }

  renderList() {
    const { prices } = this.state;
    if(prices.length === 0) {
      return <p>R 0.00</p>
    }
    return prices.map((value, index) => {
      return <p onClick={() => this.handleDelete(index)} key={index}>R {parseFloat(value).toFixed(2)}</p>
    })
  }

  render() {
    const { prices } = this.state;
    let total = 0;
    prices.forEach((value) => {
      total+=parseFloat(value)
    })

    return (
      <div className="App">
        <div className="register">
          <div className="screen">
              <p>R {parseFloat(this.state.screenamount).toFixed(2)}</p>
          </div>
          <div className="keys">
            <span onClick={this.handlePress("1")}>1</span>
            <span onClick={this.handlePress("2")}>2</span>
            <span onClick={this.handlePress("3")}>3</span>
            <span onClick={this.handlePress("4")}>4</span>
            <span onClick={this.handlePress("5")}>5</span>
            <span onClick={this.handlePress("6")}>6</span>
            <span onClick={this.handlePress("7")}>7</span>
            <span onClick={this.handlePress("8")}>8</span>
            <span onClick={this.handlePress("9")}>9</span>
            <span role="img" aria-label="del" onClick={() => this.handlePop()}>❌</span>
            <span onClick={this.handlePress("0")}>0</span>
            <span role="img" aria-label="tick" onClick={() => this.handleAppend()}>✔️</span>
          </div>
      </div>
      <div className="results">
        {this.renderList()}
        <p className="answer">R {parseFloat(total).toFixed(2)}</p>
      </div>
      </div>
    );
  }
}

export default App;
